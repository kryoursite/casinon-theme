<?php

/**
 *
 * Please do not make any edits to this file. All edits should be done in a child theme.
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

//Define theme version
define('CASINON_VERSION', wp_get_theme()->get('Version'));

//Define template directory
define('CASINON_TEMPLATE_DIR', get_template_directory());

//Define template directory uri
define('CASINON_DIR_URI', get_template_directory_uri());

define('DEVELOPMENT', false);

//Include theme setup files
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-setup.php';

//Helper functions
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-casinos-cpt.php';

//Helper functions
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-custom-cpt.php';

//General code for theme - scripts, styles
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-general.php';

//Helper functions
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-helper-functions.php';

//Include ACF setup files
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-acf-setup.php';

//Include this file if remove slug option is enabled
if (get_field('casinon_casino_remove_slug', 'options')) {
    require_once CASINON_TEMPLATE_DIR . '/inc/casinon-remove-casino-slug.php';
}

//Include ACF fields for theme
require_once CASINON_TEMPLATE_DIR . '/inc/casinon-acf-fields.php';