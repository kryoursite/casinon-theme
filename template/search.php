<?php

/**
 * The template for displaying search results.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach ($query_args as $key => $string) {
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
}

$the_query = new WP_Query($search_query);

if ($the_query->have_posts()) : ?>
    <main class="site-main" role="main">

        <div class="page-content">
            <div class="container">
                <span class="search-title"><?php _e('Search results for:', 'casinon'); ?> <?php echo $search_query["s"]; ?> </span>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="search-results">
                        <div class="search-single-post my-20">
                            <?php if (has_post_thumbnail()) : ?>
                                <div class="search-single-thumb">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                            <?php endif; ?>
                            <div class="search-single-information">
                                <a class="search-single-title" href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                                <div class="search-single-date">
                                    <?php _e('Published: ', 'casinon') ?><span><?php echo get_the_date("d-m-y"); ?></span>
                                </div>

                                <div class="search-single-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="seperator"><img src="<?php echo CASINON_DIR_URI . '/dist/img/diamond.png' ?>" alt="seperator"></div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </main>
<?php else : ?>
    <main class="site-main" role="main">
        <div class="page-content">
            <div class="container">
                <span class="search-title"><?php _e('Search results for:', 'casinon'); ?> <?php echo $search_query["s"]; ?> </span>
                <div class="search-results">

                    <span class="no-results"><?php _e('Sorry, we couldnt find anything. Try searching with different keyowords.', 'casinon'); ?></span>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>
<?php wp_reset_postdata(); ?>