<?php

/**
 * The template for displaying single pages.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<?php
while (have_posts()) : the_post();
?>
    <main class="site-main" role="main">
        <div class="page-content">
            <?php if (have_rows('casinon_flexible_content')) : ?>
                <?php while (have_rows('casinon_flexible_content')) : the_row(); ?>
                    <?php if (get_row_layout() == 'one_column') : ?>
                        <?php $bg_img = get_sub_field('flexible_content_background_1col'); ?>
                        <?php $content_single_col = get_sub_field('flexible_content_single_col'); ?>
                        <?php $bg_col = get_sub_field('flexible_content_background_color'); ?>
                        <?php $content_text_col = get_sub_field('flexible_content_text_color'); ?>
                        <div class="page-block" style="background-image:url(<?php echo $bg_img; ?>);color:<?php echo $content_text_col ?>;background-color:<?php echo $bg_col; ?>">
                            <div class="one-col-wrap container <?php if (get_sub_field('enable_block_sidebar')) echo "with-sidebar"; ?>">
                                <div class="single-column">
                                    <?php echo $content_single_col; ?>
                                </div>
                                <?php if (get_sub_field('enable_block_sidebar')) : ?>
                                    <aside class="sidebar">
                                        <?php dynamic_sidebar(get_sub_field('flexible_content_sidebar')); ?>
                                    </aside>
                                <?php endif; ?>
                            </div>
                        </div>

                    <?php elseif (get_row_layout() == 'two_columns') : ?>
                        <?php $two_col_bg_img = get_sub_field('flexible_content_background_2col'); ?>
                        <?php $two_col_bg_color = get_sub_field('flexible_content_background_color_2col'); ?>
                        <?php $two_col_text_color = get_sub_field('flexible_content_text_color_2col'); ?>
                        <?php $two_col_left_col = get_sub_field('flexible_content_two_col_1'); ?>
                        <?php $two_col_right_col = get_sub_field('flexible_content_two_col_2'); ?>
                        <div class="page-block" style="background-image:url(<?php echo $two_col_bg_img; ?>);color:<?php echo $two_col_text_color; ?>;background-color:<?php echo $two_col_bg_color; ?>">
                            <div class="two-col-wrap container">
                                <div class="left-column">
                                    <?php echo $two_col_left_col; ?>
                                </div>
                                <div class="right-column">
                                    <?php echo $two_col_right_col; ?>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>
                <?php endwhile; ?>
            <?php else : ?>
                <div class="page-block">
                    <div class="container">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if (!is_front_page()) : ?>
            <?php get_template_part('template-parts/author-box'); ?>
        <?php endif; ?>
    </main>

<?php
endwhile;
