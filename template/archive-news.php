<?php

/**
 * The template for displaying archives.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$author_id = get_the_author_meta('ID');
$author_name = get_field('casinon_author_name', 'user_' . $author_id);
$author_image = get_field('casinon_author_image', 'user_' . $author_id);

?>

<main class="site-main" role="main">

    <div id="ajax-posts" class="archive-content">
        <div class="container">
            <h1 class="my-10"><?php _e('News Archive', 'casinon') ?></h1>
            <div class="inner-archive-content">
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'post_type' => 'news',
                    'posts_per_page' => 18,
                    'paged' => $paged,
                );

                $news_posts = new WP_Query($args);

                if ($news_posts->have_posts()) :
                    while ($news_posts->have_posts()) : $news_posts->the_post(); ?>

                        <?php get_template_part('template-parts/news-archive-post'); ?>

                <?php endwhile;
                else :
                    _e('Sorry, no posts found', 'casinon');
                endif;
                ?>
            </div>

            <div class="nav-links">
                <?php
                $total_pages = $news_posts->max_num_pages;
                if ($total_pages > 1) {
                    $current_page = max(1, get_query_var('paged'));
                    echo paginate_links(array(
                        'base' => get_pagenum_link(1) . '%_%',
                        'format' => '/page/%#%',
                        'current' => $current_page,
                        'total' => $total_pages,
                        'prev_text'    => false,
                        'next_text'    => false,
                    ));
                }
                ?>
            </div>

        </div>
    </div>
</main>