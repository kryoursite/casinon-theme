<?php

/**
 * The template for displaying casino archive
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<main class="site-main" role="main">
    <div class="container">

        <h1><?php the_field('casino_archive_title', 'options'); ?></h1>

        <?php the_field('archive_content_top', 'options'); ?>

        <div class="casino-archive">

            <div class="archive-box-wrap">

                <?php

                $args = array(
                    'post_type' => 'casino',
                    'posts_per_page' => -1,
                );
                $query = new WP_Query($args);

                ?>
                <div class="box-wrap">

                    <?php if ($query->have_posts()) : ?>

                        <?php while ($query->have_posts()) : $query->the_post(); ?>

                            <div class="archive-box">

                                <div class="archive-box__image" style="<?php if (get_the_post_thumbnail()) echo 'padding:20px 0;'; ?>">
                                    <?php the_post_thumbnail(); ?>
                                </div>

                                <div class="archive-box__bonus">
                                    <?php the_field('casinon_casino_bonus_line'); ?>
                                </div>
                                <div class="archive-box__button">
                                    <a href="<?php the_field('casinon_casino_redirect_link'); ?>"><?php _e('Get Bonus', 'casinon'); ?></a>
                                </div>
                                <a class="archive-box__goto" href="<?php the_permalink(); ?>">
                                    <?php _e('Casino Review', 'casinon'); ?>
                                </a>
                            </div>

                        <?php endwhile; ?>

                        <?php wp_reset_postdata(); ?>

                    <?php else : ?>

                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

                    <?php endif; ?>

                </div>

            </div>

        </div>

        <?php the_field('archive_content_top', 'options'); ?>

    </div>

</main>