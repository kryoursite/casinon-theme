<?php

/**
 * The template for displaying author page.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

$curauth_id = $curauth->ID;

$author_name = get_field('casinon_author_name', 'user_' . $curauth_id);

$author_information = get_field('casinon_author_information', 'user_' . $curauth_id);

$author_image = get_field('casinon_author_image', 'user_' . $curauth_id);

$author_twitter = get_field('casinon_author_twitter', 'user_' . $curauth_id);

$author_linkedin = get_field('casinon_author_linkedin', 'user_' . $curauth_id);

$author_instagram = get_field('casinon_author_instagram', 'user_' . $curauth_id);

?>

<main class="site-main" role="main">
    <div class="author-content container">
        <div class="author-box" <?php if (!$author_image) echo 'style="margin:40px auto"'; ?>>
            <div class="inner-author-box">
                <?php if ($author_image) : ?>
                    <img class="author-img" src="<?php echo $author_image["url"]; ?>" alt="<?php echo $author_image["alt"]; ?>">
                <?php endif; ?>
                <span class="author-name"><?php echo $author_name; ?></span>
                <p class="author-information">
                    <?php echo $author_information; ?>
                </p>
                <div class="author-socials">
                <?php if ($author_twitter) : ?>
                        <a href="<?php echo $author_twitter; ?>"><img class="twitter-icon" height="20" width="20" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/twitter.png' ?>" alt="twitter-logo"></img></a>
                    <?php endif; ?>
                    <?php if ($author_linkedin) : ?>
                        <a href="<?php echo $author_linkedin; ?>"><img class="twitter-icon" height="20" width="20" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/linkedin.png' ?>" alt="linkedin-logo"></img></a>
                    <?php endif; ?>
                    <?php if ($author_instagram) : ?>
                        <a href="<?php echo $author_instagram; ?>"><img class="inst-icon" height="20" width="20" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/inst.png' ?>" alt="inst-logo"></img></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="author-work my-20">
            <h3><?php _e('Author Posts', 'casinon'); ?></h3>
            <div class="author-posts my-20">
                <?php casinon_generate_author_work($curauth_id, "news") ?>
            </div>
        </div>
    </div>
</main>