<?php

/**
 * The template for displaying casino reviews.
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>


<div class="casino-header" style='background-image:url(<?php echo CASINON_DIR_URI . '/dist/img/desktop-layout.png'; ?>)'>
    <?php get_template_part('template-parts/casino/casino-header'); ?>
</div>

<?php get_template_part('template-parts/header-block-parts/page-navigation'); ?>

<main class="site-main" role="main">
    <div class="casino-content">
        <?php get_template_part('template-parts/casino/casino-content'); ?>
    </div>
</main>