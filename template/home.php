<?php

/**
 * The template for displaying post archive(home template)
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<main class="site-main" role="main">
    <div class="home-content container">
        <h1 class="my-10"><?php _e('Blog Page', 'casinon') ?></h1>
        <div class="inner-home-content">
            <div class="single-column">
                <?php
                if (have_posts()) :
                    while (have_posts()) : the_post(); ?>

                        <div class="home-single-post my-20">
                            <?php if (has_post_thumbnail()) : ?>
                                <div class="home-single-thumb">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                            <?php endif; ?>
                            <div class="home-single-information">
                                <a class="home-single-title" href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a>
                                <div class="home-single-date">
                                    <?php _e('Published: ', 'casinon') ?><span><?php echo get_the_date("d-m-y"); ?></span>
                                </div>

                                <div class="home-single-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="seperator"><img src="<?php echo CASINON_DIR_URI . '/dist/img/diamond.png' ?>" alt="seperator"></div>

                <?php endwhile;
                else :
                    _e('Sorry, no posts found', 'casinon');
                endif;
                ?>
            </div>
        </div>
</main>