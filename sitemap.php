<?php

/**
 * Template Name: Sitemap Template
 */
get_header(); ?>



<div class = "sitemap-body">
            <h1 class="sitemap-h1"><?php the_title(); ?></h1>

<div class = "body-row  container">
	<h2 class="sitemap-title" id="pages"><?php _e('Pages', 'casinon'); ?></h2>
	<ul>
	<?php
	// Add pages you'd like to exclude in the exclude here
	wp_list_pages(
	  array(
	    'exclude' => '',
	    'title_li' => '',
		'depth' => 1,	
	  )
	);
	?>
	</ul>
</div>

<div class = "body-row  container">

<h2 class="sitemap-title" id="sitemap-our-work"><?php _e('Posts', 'casinon'); ?></h2>
<ul>
<?php 
$postsArgs = array(
	'post_type' => 'news',
	'posts_per_page'=>'-1',
	//'post__not_in' => array(), 
);
$postsLoop = new WP_Query( $postsArgs );
while ( $postsLoop->have_posts() ) {
	$postsLoop->the_post();
?>
	<li <?php post_class(); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php } wp_reset_query(); ?>
</ul>
</div>

<div class = "body-row  container">

<h2 class="sitemap-title" id="sitemap-our-work"><?php _e('Casino reviews', 'casinon'); ?></h2>
<ul>
<?php 
$postsArgs = array(
	'post_type' => 'casino',
	'posts_per_page'=>'-1',
	//'post__not_in' => array(), 
);
$postsLoop = new WP_Query( $postsArgs );
while ( $postsLoop->have_posts() ) {
	$postsLoop->the_post();
?>
	<li <?php post_class(); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php } wp_reset_query(); ?>
</ul>
</div>


<div class = "body-row  container">
	<h2 class="sitemap-title" id="XML"><?php _e('XML Sitemap', 'casinon'); ?></h2>
    <ul>
    	<li>
        <a href="https://www.casinon.com/sitemap_index.xml">https://www.casinon.com/sitemap_index.xml</a>
    	</li>
    </ul>
</div>
</div>

<?php get_footer(); ?>