<?php

/**
 * The site's entry point.
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();


if (is_singular('page')) {
    //If is page load page template
    get_template_part('template/page');
} elseif (is_singular('post') || is_singular('news') || is_singular('games')) {
    //If is post load post template
    get_template_part('template/post');
} elseif (is_singular('casino')) {
    //If is casino load casino template
    get_template_part('template/casino');
} elseif (is_author()) {
    //If is casino load casino template
    get_template_part('template/author');
} elseif (is_home()) {
    //If is casino load casino template
    get_template_part('template/home');
} elseif (is_post_type_archive('casino')) {
    //If is archive load archive template
    get_template_part('template/archive-casino');
} elseif (is_archive()) {
    //If is archive load archive template
    get_template_part('template/archive-news');
} elseif (is_search()) {
    //If is search load search template
    get_template_part('template/search');
} else {
    get_template_part('template/404');
}

get_footer();
