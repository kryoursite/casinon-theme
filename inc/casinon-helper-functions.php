<?php

/**
 * Helper functions.
 *
 * @package Casinon
 */


/**
 * Helper function to add correct margin for side navigation 
 */
function casinon_sidenav_margin()
{
    if (is_admin_bar_showing()) {
        echo "style='margin-top:92px';";
    } else {
        echo "style='margin-top:60px;'";
    }
}

/**
 * Custom excerpt lenght
 */

function casinon_custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'casinon_custom_excerpt_length', 999);

/**
 * Create footer menu
 */

function casinon_generate_footer_menu($name)
{
    $args = array(
        'theme_location' => "$name",
        'container' => 'div',
        'echo' => false,
    );

    $menu = "";

    $menu .= "<div class='footer-column'>";

    $menu .= "<span>";

    $menu .= wp_get_nav_menu_name($name);

    $menu .= "</span>";

    $menu .= wp_nav_menu($args);

    $menu .= "</div>";

    return $menu;
}

/**
 * Function that adds person schema to pages and posts
 */

function casinon_person_schema()
{
    $author_id = get_the_author_meta('ID');

    $author_name = get_field('casinon_author_name', 'user_' . $author_id);

    $knows_about = get_field('casinon_author_knows_about', 'user_' . $author_id);

    $schema = array(
        "@context" => "http://schema.org",
        "@type" => "Person",
        "name" => $author_name,
        "knowsAbout" => $knows_about,
    );

    if (is_singular('page') || is_singular('post'))
        echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
}

add_action('wp_head', 'casinon_person_schema');

/**
 * Function that generates author content for author page
 */


function casinon_generate_author_work($id, $type)
{
    $args = array(
        'post_type' => $type,
        'author' => $id,
        'posts_per_page' => 12,
    );

    $loop = new WP_Query($args);

    if ($loop->have_posts()) {
        while ($loop->have_posts()) {
            $loop->the_post();
            get_template_part('template-parts/author-page/author-posts');
        } // end while
    } // end if
    wp_reset_postdata();
}

/**
 * Function that loads more posts for news custom post type
 */

function load_more_posts()
{
    $paged = $_REQUEST["page"];

    $loop_args = array(
        'post_type' => 'news',
        'posts_per_page' => 9,
        'paged' => $paged,
    );

    $loop = new WP_Query($loop_args);

    while ($loop->have_posts()) :
        $loop->the_post();
        get_template_part('template-parts/news-archive-post');
    // End the loop.

    endwhile;

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');
add_action('wp_ajax_load_more_posts', 'load_more_posts');


function filter_categories()
{
    $category = $_POST['category'];

    $loop_args = array(
        'post_type' => 'casino',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'casino-category',
                'field' => 'term_id',
                'terms' => $category,
                'operator' => 'IN',
            )
        ),
    );

    $loop = new WP_Query($loop_args);

    while ($loop->have_posts()) :
        $loop->the_post();
        get_template_part('template-parts/casino/toplist-templates/toplist-base');
    // End the loop.
    endwhile;

    wp_reset_postdata();

    die();
}

add_action('wp_ajax_nopriv_filter_categories', 'filter_categories');
add_action('wp_ajax_filter_categories', 'filter_categories');
