<?php

/**
 * News CPT, Games CPT 
 *
 * @package Casinon
 */

function casinon_news_init()
{
    $labels = array(
        'name'                  => _x('News', 'Post type general name', 'casinon'),
        'singular_name'         => _x('Casino nyheter', 'Post type singular name', 'casinon'),
        'menu_name'             => _x('News', 'Admin Menu text', 'casinon'),
        'name_admin_bar'        => _x('News', 'Add New on Toolbar', 'casinon'),
    );

    $args = array(
        'labels' => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'nyheter'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-rss',
        'supports'           => array('title', 'editor', 'author', 'thumbnail'),
    );

    register_post_type('news', $args);
}

add_action('init', 'casinon_news_init');


function casinon_games_init()
{
    $labels = array(
        'name'                  => _x('Casinospel', 'Post type general name', 'casinon'),
        'singular_name'         => _x('Casinospel', 'Post type singular name', 'casinon'),
        'menu_name'             => _x('Casinospel', 'Admin Menu text', 'casinon'),
        'name_admin_bar'        => _x('Casinospel', 'Add New on Toolbar', 'casinon'),
    );

    $args = array(
        'labels' => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'casinospel'),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-games',
        'supports'           => array('title', 'editor', 'author', 'thumbnail'),
    );

    register_post_type('games', $args);
}

add_action('init', 'casinon_games_init');
