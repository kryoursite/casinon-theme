<?php

/**
 * Theme Setup functions
 * 
 * @package Casinon
 */

if (!function_exists('casinon_setup')) :
    add_action('after_setup_theme', 'casinon_setup');
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     */
    function casinon_setup()
    {

        /**
         * Make theme available for translation.
         * Translations can be placed in the /languages/ directory.
         */
        load_theme_textdomain('casinon', get_template_directory() . '/languages');

        /**
         * Add theme support for various features.
         */
        add_theme_support('post-thumbnails');
        add_theme_support('custom-logo');
        add_theme_support('title-tag');
        add_theme_support('post-formats', array('quote', 'image', 'video'));
        add_theme_support('html5', array('search-form', 'caption', 'script', 'style'));
        add_theme_support('responsive-embeds');

        /**
         * Add support for two custom navigation menus.
         */
        register_nav_menus(array(
            'main-menu'   => 'Main Menu',
            'mobile-menu' => 'Mobile Menu',
            'footer-menu-1' => 'Footer Menu 1',
            'footer-menu-2' => 'Footer Menu 2',
            'footer-menu-3' => 'Footer Menu 3',
            'footer-menu-4' => 'Footer Menu 4',
            'footer-menu-5' => 'Footer Menu 5',
        ));

        register_sidebar(array(
            'name'          => __('Sidebar - 1', 'casinon'),
            'id'            => 'sidebar-1',
            'description'   => __('Custom sidebar', 'casinon'),
        ));

        register_sidebar(array(
            'name'          => __('Sidebar - 2', 'casinon'),
            'id'            => 'sidebar-2',
            'description'   => __('Custom sidebar', 'casinon'),
        ));
    }
endif;

/**
 *  Remove editor / thumbnails from page post type
 */

function casinon_remove_support()
{
    $post_type = 'page';
    remove_post_type_support($post_type, 'thumbnail');
}
add_action('init', 'casinon_remove_support', 100);
