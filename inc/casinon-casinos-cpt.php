<?php

/**
 * Casinos CPT 
 *
 * @package Casinon
 */

/**
 * Register casino CPT 
 */

function casinon_casinos_init()
{
    $labels = array(
        'name'                  => _x('Casinos', 'Post type general name', 'casinon'),
        'singular_name'         => _x('Casino', 'Post type singular name', 'casinon'),
        'menu_name'             => _x('Casinos', 'Admin Menu text', 'casinon'),
        'name_admin_bar'        => _x('Casino', 'Add New on Toolbar', 'casinon'),
    );

    $args = array(
        'labels' => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'casino'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-welcome-add-page',
        'supports'           => array('title', 'editor', 'author', 'thumbnail', 'revisions'),
    );

    register_post_type('casino', $args);
}

add_action('init', 'casinon_casinos_init');

/**
 * Register casino taxonomy - category 
 */
function casinon_casino_taxonomy()
{
    $args = array(
        'label'        => __('Category', 'casinon'),
        'public'       => false,
        'show_ui'   => true,
        'rewrite'      => false,
        'hierarchical' => true
    );

    register_taxonomy('casino-category', 'casino', $args);
}
add_action('init', 'casinon_casino_taxonomy');



//Changes the slug of the custom post type
function casinon_change_casino_slug($args, $post_type)
{

    if ('casino' === $post_type && get_field('casinon_casino_change_slug', 'options')) {
        $args['rewrite']['slug'] = get_field('casinon_casino_change_slug', 'options');
    }

    return $args;
}

add_filter('register_post_type_args', 'casinon_change_casino_slug', 10, 2);

//Changes the slug of the custom post type archive page
function casinon_change_casino_archive_slug($args, $post_type)
{
    if ('casino' === $post_type && get_field('casino_archive_slug', 'options')) {
        $args['has_archive'] = get_field('casino_archive_slug', 'options');
    }
    return $args;
}

add_filter('register_post_type_args', 'casinon_change_casino_archive_slug', 10, 2);

/**
 * Add schema for casino pages
 */
function casinon_add_casino_schema()
{

    $logo = get_theme_mod('custom_logo');
    if ($logo) {
        $image = wp_get_attachment_image_src($logo, 'full');
        $image_url = $image[0];
    }

    $input = get_site_url();

    // in case scheme relative URI is passed, e.g., //www.google.com/
    $input = trim($input, '/');

    // If scheme not included, prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }

    $url_parts = parse_url($input);

    // remove www
    $domain = preg_replace('/^www\./', '', $url_parts['host']);

    if (get_field('casinon_casino_toplist_rating')) {
        $rating = get_field('casinon_casino_toplist_rating');
    } else {
        $rating = 4.5;
    }

    if (is_singular('casino')) {
        $schema = array(
            '@context'  => "http://schema.org",
            '@type'     => "Review",
            'datePublished' => get_the_date(),
            'dateModified' => get_the_modified_date(),
            'itemReviewed' => array(
                '@type'   => "Game",
                'name'   => get_the_title(),
                'image'  => get_the_post_thumbnail_url(),
                'url'    => get_the_permalink(),
            ),
            'author' => array(
                '@type' => "Organization",
                'name' => $domain,
                'logo'  => $image_url,
            ),
            'reviewRating' => array(
                '@type' => "Rating",
                'ratingValue' => $rating,
                'bestRating' => 5,
            )
        );
        echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
    }
}

add_action('wp_head', 'casinon_add_casino_schema');


/**
 * Casino toplist shortcode
 */
function casino_toplist($atts)
{
    $atts = shortcode_atts(
        array(
            'limit' => 15,
            'id'  => '',
            'updated' => '',
            'categories' => '',
            'style' => '',
        ),
        $atts,
        'casino_toplist'
    );

    $id = $atts['id'];
    $id = explode(',', $id);

    $loop_args = array(
        'post_type' => 'casino',
        'posts_per_page' => $atts['limit'],
        'orderby' => 'post__in',
    );

    if (!empty($atts['id'])) {
        $loop_args['post__in'] = $id;
    }

    $loop = new WP_Query($loop_args);

    ob_start();
?>
    <?php if ($atts['updated']) : ?>
        <div class="toplist-updated">
            <span><?php _e('Toplist updated:', 'casinon'); ?></span>
            <span class="toplist-date"><?php echo date_i18n("F, Y"); ?>
            </span>
        </div>
    <?php endif; ?>

    <?php if ($atts['categories']) : ?>
        <div class="toplist-categories">
            <?php
            $taxonomies = get_terms(array('taxonomy' => 'casino-category', 'hide_empty' => true));
            foreach ($taxonomies as $tax) {
                $taxonomy_img = get_field('casinon_casino_category_image', $tax->taxonomy . '_' . $tax->term_id);
                $output = "<div class='single-cat'>";
                if ($taxonomy_img) {
                    $output .= "<img class='tax-img' src={$taxonomy_img['url']} height={$taxonomy_img['height']} width={$taxonomy_img['width']} alt={$taxonomy_img['alt']}>";
                }
                $output .= "<span data-category='{$tax->term_id}' class='button-filter'>" . esc_attr($tax->name) . '</span>';
                $output .= "</div>";
                echo $output;
            }

            ?>
        </div>
    <?php endif; ?>
    <?php  if ($atts['style'] === '2') { ?>
                <div class="casino-list2">
        <?php } elseif ($atts['style'] === '3') { ?>
                <div class="casino-list3">
        <?php } elseif ($atts['style'] === '4') { ?>
                <div class="casino-list4">
        <?php } else { ?>
                <div class="casino-list">
        <?php } ?>

        <?php
        while ($loop->have_posts()) :

            $loop->the_post();
            
              if ($atts['style'] === '2') {
                include CASINON_TEMPLATE_DIR .  '/template-parts/casino/toplist-templates/toplist-base2.php';
            } elseif ($atts['style'] === '3') {
                include CASINON_TEMPLATE_DIR .  '/template-parts/casino/toplist-templates/toplist-base3.php';
            } elseif ($atts['style'] === '4') {
                include CASINON_TEMPLATE_DIR .  '/template-parts/casino/toplist-templates/toplist-base4.php';
            }else {
                include CASINON_TEMPLATE_DIR .  '/template-parts/casino/toplist-templates/toplist-base.php';
            }
        endwhile;

        wp_reset_postdata();

        ?>
    </div>
<?php
    return ob_get_clean();
}

add_shortcode('casino_list', 'casino_toplist');
