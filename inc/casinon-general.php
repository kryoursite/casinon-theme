<?php

/**
 * General functions.
 *
 * @package Casinon
 */

/**
 * Register and Enqueue Styles.
 */



function casinon_register_styles()
{
    $css_path = CASINON_TEMPLATE_DIR . '/dist/css/style.css';

    if (DEVELOPMENT) {
        wp_enqueue_style('casinon-style', CASINON_DIR_URI . '/dist/css/style.css', array(), filemtime($css_path));
        wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;700&display=swap');
    } else {
        wp_enqueue_style('casinon-style', CASINON_DIR_URI . '/dist/css/style.css', array(), CASINON_VERSION);
        wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;700&display=swap');
    }
}

add_action('wp_enqueue_scripts', 'casinon_register_styles');

add_filter ( 'wp_resource_hints' ,'resourceHints', 10, 2);

function resourceHints($hints, $relation_type)
    {
        $mpq_css = 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap';
        $font_css = 'https://fonts.googleapis.com/css?family=Poppins%3A400%2C700&display=swap';
        $fa_css = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css';
        $fonts = 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&family=Roboto:wght@300;400;700&family=Noto+Sans+JP:wght@300;400;700&family=Noto+Sans+JP%3Awght%40300%3B400%3B700&ver=5.6.2&display=swap';
        switch ($relation_type) {
            case 'prerender':
                $hints[] = $mpq_css;
                $hints[] = $font_css;
                $hints[] = $fa_css;
                $hints[] = $fonts;
                break;
            case 'prefetch':
                $hints[] = $mpq_css;
                $hints[] = $font_css;
                $hints[] = $fa_css;
                $hints[] = $fonts;
                break;
            case 'preconnect' :
                $hints[] = '//fonts.googleapis.com';
                $hints[] = '//fonts.gstatic.com';
                $hints[] = '//a.omappapi.com';
                $hints[] = '//google-analytics.com';
                $hints[] = '//js-agent.newrelic.com';
                $hints[] = '//bam.eu01.nr-data.net';
                $hints[] = '//cdnjs.cloudflare.com';
                break;
        }
        return $hints;
    }


add_filter ( 'wp_head' , 'preload_fonts', 3);

function preload_fonts(){
    $preload_hints = [
    'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap&ver=' . get_bloginfo( 'version' ),
    'https://fonts.googleapis.com/css?family=Poppins%3A400%2C700&display=swap&ver=' . get_bloginfo( 'version' ),
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css?ver=2.0.13',
    'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;700&display=swap',
    'https://fonts.googleapis.com/css2?family=Noto+Sans+JP%3Awght%40300%3B400%3B700&ver=5.6.2&display=swap',
    ];
    $output = '';
    foreach ($preload_hints as $hint) {
        $output .= '<link rel="preload" href="'.$hint.'" as="style" />';
    }
    echo $output;
}

/**
 * Register and Enqueue Scripts.
 */
function casinon_register_scripts()
{
    $js_path = CASINON_TEMPLATE_DIR . '/dist/js/scripts.js';

    if (DEVELOPMENT) {
        wp_enqueue_script('casinon-scripts', CASINON_DIR_URI . '/dist/js/scripts.js', array('jquery'), filemtime($js_path), false);
        wp_script_add_data('casinon-scripts', 'async', true);
        wp_localize_script('casinon-scripts', 'wp_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    } else {
        wp_enqueue_script('casinon-scripts', CASINON_DIR_URI . '/dist/js/scripts.js', array('jquery'), CASINON_VERSION, false);
        wp_script_add_data('casinon-scripts', 'async', true);
        wp_localize_script('casinon-scripts', 'wp_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    }
    
}

add_action('wp_enqueue_scripts', 'casinon_register_scripts');

//add_filter( 'script_loader_tag', 'mind_defer_scripts', 10, 3 );
function mind_defer_scripts( $tag, $handle, $src ) {
    $defer = array( 
      'casinon-scripts','wp-embed'
    );
    if ( in_array( $handle, $defer ) ) {
       return '<script src="' . $src . '" async type="text/javascript" id="'.$handle.'-js"></script>' . "\n";
    }
      
      return $tag;
  } 


/**
 * Function to add images in front of menu items
 */
function casinon_nav_menu_objects($items, $args)
{
    // loop
    foreach ($items as &$item) {
        // vars
        $icon = get_field('casinon_menu_icon', $item);
        // append icon
        if ($icon) {
            $item->title = "<img src='{$icon["url"]}' alt='{$icon["alt"]}' class='menu-icon'>" . "<span>" . $item->title . "</span>";
        }
    }
    return $items;
}
add_filter('wp_nav_menu_objects', 'casinon_nav_menu_objects', 10, 2);
