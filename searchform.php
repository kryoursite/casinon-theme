<form id="searchform" class="p-20" method="get" action="<?php echo esc_url(home_url('/')); ?>">
    <input type="text" class="search-field px-10" name="s" placeholder="Search..." value="<?php echo get_search_query(); ?>">
    <button type="submit"> 
        <i class="search-button">
            <div class="search__circle search-white-circle"></div>
            <div class="search__rectangle search-white"></div>
        </i>
   </button>
</form>