<?php

/**
 * Single page block content part
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="page-title">
    <h1><?php the_title(); ?></h1>
</div>
<div class="page-information">
    <div class="breadcrumbs">
        <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
    </div>
    <div class="info-separator mx-10"><i class="fas fa-grip-lines-vertical"></i></div>
    <div class="last-update">
        <span><?php _e('Updated: ', 'casinon'); ?><?php the_modified_date("d/m/Y"); ?></span>
    </div>
</div>
<?php if (get_field('casinon_enable_page_header_block_text')) : ?>
    <div class="header-block-text my-10">
        <?php the_field('casinon_page_header_block_text'); ?>
    </div>
<?php endif; ?>