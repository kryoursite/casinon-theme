<?php

/**
 * Front page block content part
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="header-block__left">
    <div class="page-title">
        <h1><?php the_title(); ?></h1>
    </div>
    <div class="header-block-text">
        <?php if (get_field("casinon_front_page_intro_text")) : ?>
            <?php the_field("casinon_front_page_intro_text"); ?>
        <?php endif; ?>
    </div>
    <div class="header-block-more">
        <span><?php _e('Page Content', 'casinon'); ?><i class="arrow arrow-down arrow-header-block"></i> </span>
        <ul class="front-nav" data-toc data-toc-headings="h2"></ul>
    </div>
</div>
<div class="header-block__right">

    <?php

    if (have_rows('casinon_front_page_casino_box')) :

        while (have_rows('casinon_front_page_casino_box')) : the_row();

            $box_img = get_sub_field("casino_box_img");
            $box_title = get_sub_field("casino_box_title");
            $box_extra_field = get_sub_field("casino_box_extra field");
            $page = get_sub_field("casino_box_page");
    ?>
            <a class="games-box" href="<?php echo $page; ?>">
                <img class="games-box__image" src="<?php echo $box_img["url"]; ?>" width="<?php echo $box_img["width"]; ?>" height="<?php echo $box_img["height"]; ?>"  alt="<?php $box_img["alt"]; ?>">
                <span class="games-box__title my-10">
                    <?php echo $box_title ?>
                </span>
                <span class="games-box__count">
                    <?php echo $box_extra_field; ?>
                </span>
            </a>
    <?php
        endwhile;

    endif;

    ?>
</div>