<?php

/**
 * Header navigation part
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="page-navigation">
    <div class="inner-page-navigation">
        <span><i class="fas fa-list-ul"></i><?php _e("Page Content:", "casinon"); ?></span>
        <ul class="page-nav-list" data-toc data-toc-headings="h2"></ul>
    </div>
</div>
<div class="mobile-navigation">
    <div class="inner-mobile-navigation">
        <span class="py-10"><?php _e('Page Content', 'casinon'); ?><i class="arrow arrow-down arrow-header-block"></i> </span>
        <ul data-toc data-toc-headings="h2"></ul>
    </div>
</div>