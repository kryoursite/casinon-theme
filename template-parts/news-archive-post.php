<?php

$author_id = get_the_author_meta('ID');
$author_name = get_field('casinon_author_name', 'user_' . $author_id);
$author_image = get_field('casinon_author_image', 'user_' . $author_id);

?>

<div class="archive-single-post my-20">
    <?php if (has_post_thumbnail()) : ?>
        <div class="archive-single-thumb">
            <?php the_post_thumbnail(); ?>
        </div>
    <?php endif; ?>
    <div class="archive-single-information">
        <a class="archive-single-title" href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
        </a>
        <div class="archive-single-author">
            <div class="archive-author-img">
                <img src="<?php echo $author_image['url']; ?>" alt="<?php echo $author_image['alt']; ?>">
            </div>
            <span class="archive-author-name">
                <?php echo $author_name; ?>
            </span>
            <?php echo "<i class='fas fa-grip-lines-vertical'></i>"; ?>
            <div class="archive-single-date">
                <?php _e('Published: ', 'casinon') ?><span><?php echo get_the_date("d-m-y"); ?></span>
            </div>
        </div>
    </div>
    <div class="archive-single-excerpt">
        <?php the_excerpt(); ?>
    </div>
</div>