<?php

/**
 * Author posts template part
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

$curauth_id = $curauth->ID;

$author_name = get_field('casinon_author_name', 'user_' . $curauth_id);

$author_image = get_field('casinon_author_image', 'user_' . $curauth_id);

?>

<div class="post-card author-posts">
    <?php if (has_post_thumbnail()) : ?>
        <div class="card-img-wrap p-10">
            <?php the_post_thumbnail(); ?>
        </div>
    <?php endif; ?>
    <div class="post-card-information p-10">
        <div class="post-card__cats">
            <?php

            $cats = get_the_category();

            foreach ($cats as $cat) {
                echo "<span>" . $cat->name . "</span>";
            }

            ?>
        </div>
        <span class="post-card__title"><?php the_title(); ?></span>
        <div class="post-card__excerpt"><?php the_excerpt(); ?></div>
        <button class="post-card__more btn"><a href="<?php the_permalink(); ?>"><?php _e('Read More', 'casinon'); ?></a></button>
    </div>
</div>