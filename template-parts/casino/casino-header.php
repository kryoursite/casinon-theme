<div class="casino-header-inner container">
    <div class="casino-information">
        <div class="casino-info-inner">
            <div class="breadcrumbs">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            </div>
            <h1 class="casino-title">
                <?php the_title(); ?>
            </h1>
            <span class="casino-modified">
                <?php _e('Last updated:', 'casinon'); ?> <?php echo get_the_modified_date("d/m/Y"); ?>
            </span>
        </div>
    </div>
    <div class="top-header">
        <div class="bonus-part">

            <div class="bonus-part-top p-20">
                <a href="<?php the_field('casinon_casino_redirect_link'); ?>"><?php the_post_thumbnail(); ?></a>

                <a href="<?php the_field('casinon_casino_redirect_link'); ?>" class="bonus-text"><?php the_field('casinon_casino_bonus_line'); ?></a>
            </div>
            <a href="<?php the_field('casinon_casino_redirect_link'); ?>" class="get-bonus p-10">
                <?php _e('Get Bonus', 'casinon'); ?>
            </a>
            </a>
        </div>
        <div class="general-information p-20">
            <table>
                <caption><img class="info-icon" width="15" height="15" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/info.png' ?>" alt="info-logo"></img> <?php _e('General Information', 'casinon'); ?></caption>
                <tbody>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📅</span> <?php _e('Year', 'casinon'); ?></th>
                        <td><?php the_field('casinon_casino_year'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📃</span> <?php _e('Licence', 'casinon'); ?></th>
                        <td class="licence-info"><span class="licence-hover">&#x2139;</span>
                            <p class="licence-text"><?php the_field('casinon_casino_licence'); ?></p>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row"><span class="casino-emoji">📧</span> <?php _e('Email', 'casinon'); ?></th>
                        <td class="general-email"><?php the_field('casinon_casino_email'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">💶</span> <?php _e('Minimal deposit', 'casinon'); ?></th>
                        <td><?php the_field('casinon_casino_min_deposit'); ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">⌚</span> <?php _e('Payout Time', 'casinon'); ?></th>
                        <td><?php the_field('casinon_casino_payout_time'); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bonus-information p-20">
            <table>
                <caption><img class="gift-icon" width="15" height="15" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/gift.png' ?>" alt="gift-logo"></img> <?php _e('Bonus Information', 'casinon'); ?></caption>
                <tbody>
                    <tr>
                        <th scope="row"><span class="casino-emoji">💲</span> <?php _e('Bonus', 'casinon'); ?></th>

                        <?php if (get_field('casinon_bonus_amount')) : ?>
                            <td><a href="<?php the_field('casinon_casino_redirect_link'); ?>"><?php the_field('casinon_bonus_amount'); ?></a></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">✔️</span> <?php _e('Bonus Percentage', 'casinon'); ?></th>

                        <?php if (get_field('casinon_bonus_percentage')) : ?>
                            <td><?php the_field('casinon_bonus_percentage'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>

                    <tr>
                        <th scope="row"><span class="casino-emoji">💱</span> <?php _e('Turnover', 'casinon'); ?></th>

                        <?php if (get_field('casinon_bonus_turnover')) : ?>
                            <td><?php the_field('casinon_bonus_turnover'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>

                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">🎰</span> <?php _e('Freespins', 'casinon'); ?></th>
                        <?php if (get_field('casinon_bonus_freespins')) : ?>
                            <td><?php the_field('casinon_bonus_freespins'); ?></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th scope="row"><span class="casino-emoji">📜</span> <?php _e('Bonus Code', 'casinon'); ?></th>
                        <?php if (get_field('casinon_bonus_code')) : ?>
                            <td><a class="bonus-code" href="<?php the_field('casinon_casino_redirect_link'); ?>"><?php the_field('casinon_bonus_code'); ?></a></td>
                        <?php else : ?>
                            <td>-</td>
                        <?php endif; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="bottom-header">
        <div class="bottom-header-inner">
            <?php
            $payments = get_field('casinon_payment_options');
            if ($payments) : ?>
                <div class="payment-options">
                    <span class="payments-title"><img class="card-icon" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/card.png' ?>" alt="card-logo"></img> <?php _e('Payment Options', 'casinon'); ?></span>
                    <div class="payment-images p-10">
                        <?php
                        for ($i = 0; $i < 5; $i++) {
                        ?>
                            <?php if ($payments[$i]) : ?>
                                <div class="payment-img-wrap">
                                    <img src='<?php echo CASINON_DIR_URI . "/dist/img/payments/{$payments[$i]}.png"; ?>' alt="<?php echo $payments[$i]; ?>">
                                </div>
                            <?php endif; ?>
                        <?php
                        }

                        ?>
                        <?php if (count($payments) > 5) : ?>
                            <span class="see-more"><?php _e('See All', 'casinon'); ?> <i class="arrow arrow-down arrow-casino-header-block"></i></span>

                            <div class="all-payments">
                                <?php foreach ($payments as $payment) : ?>
                                    <div class="payment-img-wrap">
                                        <img src='<?php echo CASINON_DIR_URI . "/dist/img/payments/{$payment}.png"; ?>' alt="<?php echo $payments; ?>">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php
            $software = get_field('casinon_software_options');
            if ($software) : ?>
                <div class="software">
                    <span class="software-title"><img class="arrows-icon" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/arrows.png' ?>" alt="arrows-logo"></img> <?php _e('Software', 'casinon'); ?></span>

                    <div class="software-images p-10">
                        <?php
                        for ($i = 0; $i < 5; $i++) {
                        ?>
                            <?php if ($software[$i]) : ?>
                                <div class="software-img-wrap">
                                    <img src='<?php echo CASINON_DIR_URI . "/dist/img/software/{$software[$i]}.png"; ?>' alt="<?php echo $software[$i]; ?>">
                                </div>
                            <?php endif; ?>
                        <?php
                        }

                        ?>
                        <?php
                        if (count($software) > 5) : ?>
                            <span class="see-more"><?php _e('See All', 'casinon'); ?> <i class="arrow arrow-down arrow-casino-header-block"></i></span>
                            <div class="all-software">

                                <?php foreach ($software as $soft) : ?>
                                    <div class="software-img-wrap">
                                        <img src='<?php echo CASINON_DIR_URI . "/dist/img/software/{$soft}.png"; ?>' alt="<?php echo $soft; ?>">
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>