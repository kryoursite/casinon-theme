<?php

/**
 * Simple page header block
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list2-item2 base-style2" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="margin-bottom:70px;"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list2__order2"></span>
    <div class="casino-list2__logo2">
        <a href="<?php the_field('casinon_casino_redirect_link'); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="casino-list2__bonus2"><?php the_field('casinon_casino_toplist_bonus_line'); ?></div>
    <div class="casino-list2__more2">
        <a href="<?php the_permalink(); ?>" class="list-review2"><?php _e('Casino Review', 'casinon'); ?> <i class="arrow arrow-right"></i></a>
        <a href="<?php the_field('casinon_casino_redirect_link'); ?>" class="to-the-casino2"><?php _e('Get Bonus!', 'casinon'); ?></a>
    </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list2__terms2">
        <img class="info-icon-toplists" width="10" height="10" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/info.png' ?>" alt="info-logo"></img> <span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>
