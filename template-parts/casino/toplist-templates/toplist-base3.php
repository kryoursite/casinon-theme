<?php

/**
 * Simple page header block
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list3-item3 base-style3" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="margin-bottom:70px;"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list3__order3"></span>
    <div class="casino-list3__logo3">
        <a href="<?php the_field('casinon_casino_redirect_link'); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="casino-list3__bonus3"><?php the_field('casinon_casino_toplist_bonus_line'); ?></div>
    <div class="casino-list3__rating3">
        <span class="casino-name3">
            <?php if (get_field('casinon_ratings_line')) : ?>
                <?php the_field('casinon_ratings_line'); ?>
            <?php else : ?>
                <?php the_title(); ?>
            <?php endif; ?>
        </span>
        <div class="star-rating3"><i class="star"></i></div>
        <div class="casino-rating3">
            <?php if (get_field('casinon_casino_toplist_rating')) : ?>
                <?php the_field('casinon_casino_toplist_rating'); ?>
            <?php else : ?>
                <?php echo '-'; ?>
            <?php endif; ?>

        </div>
    </div>
    <div class="casino-list3__pluses3">

        <?php
        if (have_rows('casinon_casino_top_3')) :
            while (have_rows('casinon_casino_top_3')) : the_row(); ?>
                <span class="plus3"><i><span class="plus-sign">
                            <div class="plus-sign-vertical"></div>
                            <div class="plus-sign-horizontal"></div>
                    </span></i><?php the_sub_field('top_three_line'); ?></span>
        <?php

            endwhile;

        endif;
        ?>
    </div>
    <div class="casino-list3__more3">
        <a href="<?php the_permalink(); ?>" class="list-review"><?php _e('Casino Review', 'casinon'); ?> <i class="arrow arrow-right"></i></a>
        <a href="<?php the_field('casinon_casino_redirect_link'); ?>" class="to-the-casino3"><?php _e('Get Bonus!', 'casinon'); ?></a>
    </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list3__terms3">
        <img class="info-icon-toplists" width="8" height="8" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/info.png' ?>" alt="info-logo"></img><span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>
