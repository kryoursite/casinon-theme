<?php

/**
 * Simple page header block
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="casino-list4-item4 base-style4" <?php if (get_field('enable_terms_and_conditions')) {
                                                echo 'style="margin-bottom:70px;"';
                                            } else {
                                                echo 'style="margin-bottom:50px;"';
                                            } ?>>
    <span class="casino-list4__order4"></span>
    <div class="casino-list4__logo4">
        <a href="<?php the_field('casinon_casino_redirect_link'); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="casino-list4__rating4">
        <span class="casino-name4">
            <?php if (get_field('casinon_ratings_line')) : ?>
                <?php the_field('casinon_ratings_line'); ?>
            <?php else : ?>
                <?php the_title(); ?>
            <?php endif; ?>
        </span>
        <div class="casino-list4__bonus4-1"><?php the_field('casinon_casino_toplist_bonus_line'); ?>
        </div>
        </div>


        <div class="casino-list4__pluses4">
        <div class="casino-list4__bonus4">
        <div class="star-rating4"><i class="star"></i></div>
        <div class="casino-rating4">
            <?php if (get_field('casinon_casino_toplist_rating')) : ?>
                <?php the_field('casinon_casino_toplist_rating'); ?>
            <?php else : ?>
                <?php echo '-'; ?>
            <?php endif; ?>
            </div>
        <a href="<?php the_permalink(); ?>" class="list-review"><?php _e('Casino Review', 'casinon'); ?> <i class="arrow arrow-right"></i></a>
        </div>
    <?php if (get_field('enable_terms_and_conditions')) : ?>
        <div class="casino-list4__terms4">
        <img class="info-icon-toplists" width="8" height="8" src="<?php echo CASINON_DIR_URI . '/dist/img/logos/info.png' ?>" alt="info-logo"></img><span><?php the_field('terms_and_conditions'); ?></span>
        </div>
    <?php endif; ?>
</div>
</div>