<main class="site-main" role="main">

    <div class="casino-content">
        <div class="inner-casino-content">
            <?php if (have_rows('casinon_flexible_content')) : ?>
                <?php while (have_rows('casinon_flexible_content')) : the_row(); ?>
                    <?php if (get_row_layout() == 'one_column') : ?>
                        <?php $bg_img = get_sub_field('flexible_content_background_1col'); ?>
                        <?php $content_single_col = get_sub_field('flexible_content_single_col'); ?>
                        <?php $bg_col = get_sub_field('flexible_content_background_color'); ?>
                        <?php $content_text_col = get_sub_field('flexible_content_text_color'); ?>
                        <div class="page-block" style="background-image:url(<?php echo $bg_img; ?>);color:<?php echo $content_text_col ?>;background-color:<?php echo $bg_col; ?>">
                            <div class="one-col-wrap container <?php if (get_sub_field('enable_block_sidebar')) echo "with-sidebar"; ?>">
                                <div class="single-column">
                                    <?php echo $content_single_col; ?>
                                </div>
                                <?php if (get_sub_field('enable_block_sidebar')) : ?>
                                    <aside class="sidebar">
                                        <?php dynamic_sidebar(get_sub_field('flexible_content_sidebar')); ?>
                                    </aside>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php else : ?>
                <div class="page-block">
                    <div class="container">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if (!is_front_page()) : ?>
            <?php get_template_part('template-parts/author-box'); ?>
        <?php endif; ?>
        <div class="single-casino-action hidden">
        <div class="action-wrap">
            <div class="single-ca-img">
                <a href="<?php the_field('casinon_casino_redirect_link'); ?>" rel="nofollow noopener" target="_blank"><?php the_post_thumbnail(); ?></a>
            </div>
            <div class="single-ca-info">
                <span class="ca-info"><?php the_field('casinon_casino_bonus_line'); ?></span>
                <a href="<?php the_field('casinon_casino_redirect_link'); ?>" class="ca-button" rel="nofollow noopener" target="_blank"><?php _e('Get Bonus', 'casinon'); ?></a>
            </div>
        </div>
    </div>
</main>